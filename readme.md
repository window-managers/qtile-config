# qtile-config
My configuration for qtile.

## Screenshot
![](/screenshot/qtile.png)

## qtile WM config location:
```
~/.config/qtile/config.py
```

## dwm desktop location
```
/usr/share/xsessions/qtile.desktop
```

## autostart programs script location:
```
~/.config/qtile/autostart.sh
```

## Remember to make autorun.sh executable
```
chmod +x ~/.config/qtile/autostart.sh
```

## Programs:
### Install these programs from your linux distribution package manager.
<ol>
  <li> alacritty or terminal emulator of your choice </li> 
  <li> dmenu </li>
  <li> nemo </li>
  <li> chromium/firefox </li>
  <li> redshift </li>
  <li> feh </li>
  <li> network-manager-applet </li> 
  <li> udiskie </li>
  <li> polkit-gnome/policykit-1-gnome </li>
  <li> geoclue2 </li>
  <li> pulseaudio </li>
  <li> unclutter </li>
  <li> discord </li>
  <li> vol-notify </li>
  <li> powermenu </li>
</ol>

## vol-notify
repo: https://gitlab.com/window-managers/vol-notify

## powermenu
repo: https://gitlab.com/window-managers/powermenu

## alacritty
On Ubuntu, alacritty must be compiled and installed from **cargo** or from **git**.<br>
repo: https://github.com/alacritty/alacritty

## Credit to the creators of qtile
Official Website: http://www.qtile.org/

Repository: https://github.com/qtile/qtile

## Wallpaper
### fluent wallpapers
https://github.com/vinceliuice/Fluent-gtk-theme
